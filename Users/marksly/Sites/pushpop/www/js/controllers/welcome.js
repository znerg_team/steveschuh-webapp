angular.module('pushpop.WelcomeController', [])
.controller('WelcomeController', function ($scope, $state, $timeout, $ionicPopup, $ionicLoading, $http, UserService, $ionicHistory, $location, $cordovaGeolocation) {

  $scope.getCurrentParentPage = function() {
    if ($location.path().indexOf('/mydeals') >= 0) {
      return 'mydeals';
    } else if ($location.path().indexOf('/interests') >= 0) {
      return 'interests';
    } else if ($location.path().indexOf('/profile') >= 0) {
      return 'profile';
    } else if ($location.path().indexOf('/location') >= 0) {
      return 'location';
    }
    return '';
  }

  document.addEventListener("deviceready", function() {
    // Define a div tag with id="map_canvas"
    var mapDiv = document.getElementById("map_canvas");
    mapDiv.style.height = (window.screen.height-130) + 'px';
    mapDiv.style.marginTop = '-1px';

    // Initialize the map plugin
    var map = plugin.google.maps.Map.getMap(mapDiv);

    // You have to wait the MAP_READY event.
    map.on(plugin.google.maps.event.MAP_READY, onMapInit);

    //set map to current location
    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
      var lat  = position.coords.latitude
      var long = position.coords.longitude
      console.log(lat + '   ' + long);
      var currentLocation = new plugin.google.maps.LatLng(lat, long);
      map.moveCamera({
        'target': currentLocation,
        'zoom': 17
      }, function() {
       //var mapType = plugin.google.maps.MapTypeId.HYBRID;
       //map.setMapTypeId(mapType);
       //map.showDialog();
      });
    }, function(err) {
      console.log(err);
    });
  });

  function onMapInit(map) {
    console.log('map is ready');
  }

  $scope.interests = [
    {'label': 'Item 1', 'selected': false},
    {'label': 'Item 2', 'selected': false},
    {'label': 'Item 3', 'selected': false},
    {'label': 'Item 4', 'selected': false},
    {'label': 'Item 5', 'selected': false},
    {'label': 'Item 6', 'selected': false},
    {'label': 'Item 7', 'selected': false},
    {'label': 'Item 8', 'selected': false},
    {'label': 'Item 9', 'selected': false},
    {'label': 'Item 10', 'selected': false},
  ];

  $scope.toggleActiveInterests = function(interest) {
    console.log(interest);
    interest.selected = (interest.selected) ? false : true;
  }

  $scope.selectAllInterests = function(checkbox) {
    for (var i=0;i<$scope.interests.length;i++) {
      $scope.interests[i].selected = (checkbox.toggle.checkbox.checked) ? true : false;
    }
  }

  $scope.goInterests = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true
    });
    $state.go('interests');
  }
  $scope.goLocation = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true
    });
    $state.go('location');
  }
  $scope.goWelcome = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true
    });
    $state.go('welcome');
  }
  $scope.goProfile = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true
    });
    $state.go('profile');
  }
  $scope.goMyDeals = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true
    });
    $state.go('mydeals');
  }

  //get current user
  var user = UserService.getUser();
  $scope.firstName = user.first_name;
  $scope.lastName = user.last_name;

});
