// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('pushpop', ['ionic', 'ngCordova', 'ngOpenFB', 'pushpop.LoginController', 'pushpop.WelcomeController', 'pushpop.RegisterController', 'pushpop.UserService'])

.run(function ($ionicPlatform, $rootScope, ngFB, UserService, $state) {
  ngFB.init({appId: '1658991427750614'}); //facebook app id

  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    if (toState.authenticate && !UserService.isAuthenticated()){
      // User isn’t authenticated
      $state.transitionTo("login");
      event.preventDefault();
    }
  });

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginController',
      authenticate: false
    })
    .state('register', {
      url: '/register',
      templateUrl: 'templates/register.html',
      controller: 'RegisterController',
      authenticate: false
    })
    .state('register-step1', {
      url: '/register-step1',
      templateUrl: 'templates/register-step1.html',
      controller: 'RegisterController',
      authenticate: false
    })
    .state('register-step2', {
      url: '/register-step2',
      templateUrl: 'templates/register-step2.html',
      controller: 'RegisterController',
      authenticate: false
    })
    .state('register-step3', {
      url: '/register-step3',
      templateUrl: 'templates/register-step3.html',
      controller: 'RegisterController',
      authenticate: false
    })
    .state('welcome', {
      url: '/welcome',
      templateUrl: 'templates/welcome.html',
      controller: 'WelcomeController',
      authenticate: false
    })
    .state('interests', {
      url: '/interests',
      templateUrl: 'templates/interests.html',
      controller: 'WelcomeController',
      authenticate: false
    })
    .state('location', {
      url: '/location',
      templateUrl: 'templates/location.html',
      controller: 'WelcomeController',
      authenticate: false
    })
    .state('profile', {
      url: '/profile',
      templateUrl: 'templates/profile.html',
      controller: 'WelcomeController',
      authenticate: false
    })
    .state('mydeals', {
      url: '/mydeals',
      templateUrl: 'templates/mydeals.html',
      controller: 'WelcomeController',
      authenticate: false
    });
  $urlRouterProvider.otherwise('/login');
});
