<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryDonorToDonorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('donors', function(Blueprint $table)
		{
			$table->integer('primary_donor_id')->nullable()->unsigned()->after('occupation');
		});

		Schema::table('users', function(Blueprint $table)
		{
			$table->integer('edit_rights')->after('user_status');
			$table->text('allowed_tags')->after('user_status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('donors', function(Blueprint $table)
		{
			$table->dropColumn('primary_donor_id');
		});

		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('edit_rights');
			$table->dropColumn('allowed_tags');
		});
	}

}
