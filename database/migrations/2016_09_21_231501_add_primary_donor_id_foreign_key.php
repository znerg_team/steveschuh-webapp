<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryDonorIdForeignKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('donors', function(Blueprint $table)
		{
			$table->foreign('primary_donor_id', 'primary_donor_id_fk')
				->references('id')
				->on('donors')
				->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('donors', function(Blueprint $table)
		{
			$table->dropForeign('primary_donor_id_fk');
		});
	}

}
