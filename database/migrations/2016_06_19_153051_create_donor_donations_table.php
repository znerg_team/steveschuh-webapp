<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonorDonationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('donor_donations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('donor_id')->unsigned();
			$table->timestamp('donation_date');
			$table->float('donation_amount');
			$table->foreign('donor_id')->references('id')->on('donors');
			$table->timestamps();
			$table->dropTimestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('donor_donations');
	}

}
