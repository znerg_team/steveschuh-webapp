<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateEventToDonorTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('donor_tags', function(Blueprint $table) {
			$table->date('date_event')->after('donor_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('donor_tags', function(Blueprint $table) {
			$table->dropColumn('date_event');
		});
	}

}
