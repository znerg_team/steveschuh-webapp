<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('donors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('address1');
			$table->string('address2');
			$table->string('city');
			$table->string('state');
			$table->string('country');
			$table->string('zip');
			$table->string('phone1')->nullable();
			$table->string('phone2')->nullable();
			$table->string('home_phone');
			$table->string('business_phone');
			$table->string('cellphone');
			$table->string('other_phone');
			$table->string('email')->nullable();
			$table->string('email2')->nullable();
			$table->enum('donor_type',array('company','individual'));
			$table->string('notes')->nullable();
			$table->string('employer_name');
			$table->string('occupation');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('donors');
	}

}
