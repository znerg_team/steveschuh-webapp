<?php
session_start();
print "<br> start time:" . date("H:i:s") . "<br>";

set_include_path(get_include_path() . PATH_SEPARATOR . './phpexcel/Classes/');

require_once 'PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load("UserCopModified.xlsx");
$worksheet_count = 0;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	
	$worksheet_count++;
	
	if ($worksheet_count == 1) {
		$worksheetTitle     = $worksheet->getTitle();
		//$highestRow         = $worksheet->getHighestRow(); // e.g. 10
		$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		$tagColumn 		= PHPExcel_Cell::columnIndexFromString('BV');

		$nrColumns = ord($highestColumn) - 64;
		echo "<br>The worksheet ".$worksheetTitle." has ";
		echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
		//echo ' and ' . $highestRow . ' row.';
		//echo '<br>Data: <table border="1"><tr>';
		
		$row_start = 1;
		if (isset( $_REQUEST['row_start']) ) { $row_start = $_REQUEST['row_start']; }
		$row_limit = 12941;
		if (isset( $_REQUEST['row_limit']) ) { $row_limit = $_REQUEST['row_limit']; }
		$headers = array();
		$data = array();
		
		//get header row for column names
		for ($col = 0; $col < $highestColumnIndex; ++ $col) {
					$cell = $worksheet->getCellByColumnAndRow($col, 1);
					$val = $cell->getValue();
					$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
					
					$field_name_value = $val;
					switch( $val ) {
							case "Last Name/Org": $field_name_value = "last_name"; break;
							case "First Name": $field_name_value = "first_name"; break;
							case "individualndindividualvindividualdual/companyompany": $field_name_value = "donor_type"; break;
							case "Email": $field_name_value = "email";
							case "Email2": $field_name_value = "email2";
							case "Home Phone": $field_name_value = "home_phone";
							case "Business Phone": $field_name_value = "business_phone";
							case "Cell Phone": $field_name_value = "cell_phone";
							case "Other Phone": $field_name_value = "other_phone";
							default: $field_name_value = strtolower( $val ); break;
					}
					$headers[ $col ] = $field_name_value;
		}
		
		for ($row = $row_start; $row <= $row_limit; ++ $row) {
			if ($row <= $row_limit) {
				//echo '<tr>';
				for ($col = 0; $col < $highestColumnIndex; ++ $col) {
					$cell = $worksheet->getCellByColumnAndRow($col, $row);
					$val = trim( $cell->getValue() );
					$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
					//echo '<td>' . $val . '<br>(Typ ' . $dataType . ')</td>';
					//echo '<td>' . $val . '</td>';
					
					if ($row == 1) {
						$field_name_value = $val;
						switch( $val ) {
							case "Last Name/Org": $field_name_value = "last_name"; break;
							case "First Name": $field_name_value = "first_name"; break;
							case "individualndindividualvindividualdual/companyompany": $field_name_value = "donor_type"; break;
							case "Email": $field_name_value = "email"; break;
							case "Email2": $field_name_value = "email2"; break;
							case "Home Phone": $field_name_value = "home_phone"; break;
							case "Business Phone": $field_name_value = "business_phone"; break;
							case "Cell Phone": $field_name_value = "cell_phone"; break;
							case "Other Phone": $field_name_value = "other_phone"; break;
							case "Endorsers (Y)": $field_name_value = "endorsers"; break;
							case "Clergy (CL)": $field_name_value = "clergy"; break;
							case "Financial (F)": $field_name_value = "financial"; break;
							case "Business Leader (B)": $field_name_value = "business leader"; break;
							case "AA Special (Per SRS)": $field_name_value = "aa special"; break;
							case "Political/ Activist (P)": $field_name_value = "political activist"; break;
							case "Press (PR)": $field_name_value = "press"; break;
							case "Yard/Window Sign (Y)": $field_name_value = "yard/window sign"; break;
							case "Large Yard Sign - 2014 Election Cycle (DMI To Update**)": $field_name_value = "large yard sign - 2014 election cycle"; break;
							case "Medium Yard Sign - 2014 Election Cycle (DMI To Update **)": $field_name_value = "medium yard sign - 2014 election cycle"; break;
							case "2014 Cycle Host/CoHost (Y)": $field_name_value = "2014 cycle host/cohost"; break;
							default: $field_name_value = strtolower( $val ); break;
						}
						$headers[ $col ] = $field_name_value;
					} else {
						if ($tagColumn <= $col+1) {
							$data[$row]['tags'][] = $val;
						} else {
							$data[ $row ][ $headers[ $col ] ] = $val;
						}
					}
				}
				//echo '</tr>';
			}
		}
		
		//echo '</table>';
		
		//print '<pre>' . print_r( $data, true ) . '</pre>';
		
	}
}

//connect to mysql table
$con = new mysqli('localhost', 'stevedata', 'texmex', 'stevedata');

//foreach row - insert row into the db table
$field_names = array('first_name', 'last_name', 'address1', 'city', 'state', 'country', 'zip', 'phone1', 'phone2', 'home_phone', 'business_phone', 'cellphone', 'other_phone', 'email', 'email2', 'donor_type', 'employer_name', 'occupation');
foreach( $data as $k => $d) {
	$tags = empty($d['tags']) ? [] : $d['tags'];
	$tags = parseTags($tags);

	unset($d['tags']);

	//generate fields and values
	$fields = $values = array();
	foreach( $d as $k1 => $d1 ) {
		if (in_array($k1, $field_names)) { $fields[] = $k1; $values[] = "'".$d1."'"; }
	}
	
	//created and updated fields
	$fields[] = "created_at";
	$values[] = "'" . date("Y-m-d H:i:s") . "'";
	
	$fields[] = "updated_at";
	$values[] = "'" . date("Y-m-d H:i:s") . "'";
	
	
	$fields_list = implode(",", $fields);
	$values_list = implode(",", $values);
	
	$sql 	= "insert into donors ( ".$fields_list." ) 
				values ( ".$values_list." )";
	
	//print "sql = " . $sql . "<br>";
	
	$result = $con->query($sql);
	
	$donor_id = $con->insert_id;
	
	/* donor_donations */
	$donations_fields = array( "2018 election cycle", "2014 election cycle", "2010 election cycle", "2006 election cycle");
	$donation_sql = "";
	foreach( $d as $k1 => $d1 ) {
		if (in_array($k1, $donations_fields)) { 
			$donation_sql = "insert into donor_donations ( donor_id, donation_date, donation_amount ) values ( '$donor_id', '".date("Y-01-01 00:00:00", strtotime( "January 1 " . substr( $k1, 0,4 )))."', '$d1');";
			//print "donation sql = " . $donation_sql . "<br>";
			$donation_result = $con->query( $donation_sql );
		}
	}

	/* new donor_tags */
	foreach ($tags as $tag) {
		$tagId = insertIfNotExist($con, $tag['label']);
		$now = date("Y-m-d H:i:s");
		$date_event = \DateTime::createFromFormat('m/d/y', $tag['date']);
		$dt = $date_event->format('Y-m-d');
		$donor_tag_values = sprintf("'%d','%d','%s','%s','%s'", $tagId, $donor_id, $dt, $now, $now);
		$donor_tag_sql = "INSERT INTO donor_tags (tag_id, donor_id, date_event, created_at, updated_at) values ($donor_tag_values)";
		$res = $con->query($donor_tag_sql);
	}
	
	/* donor_tags */
	/*foreach( $d as $k1 => $d1 ) {
		//print "field-name : " . $k1 . "<br/>";
		if (is_donor_tag( $k1 )) {
			if ($d1 != "") {
				$tag_id = is_donor_tag( $k1 );
				$curr_time = date("Y-m-d H:i:s");
				$donor_tag_sql = "insert into donor_tags ( tag_id, donor_id, created_at, updated_at ) values ( '$tag_id', '$donor_id', '$curr_time', '$curr_time')";
				//print "donor_tag sql = " . $donor_tag_sql . "<br>";
				$donor_tag_result = $con->query( $donor_tag_sql );
			} else {
				//print "empty donor tag value...<br>";
			}
		}
	}*/
}

mysqli_close($con);

print "<br> end time:" . date("H:i:s") . "<br>";

function parseTags(array $tags) {
	$res = [];
	foreach ($tags as $tag) {
		if (!empty($tag)) {
			$data = explode(' ', $tag);
			if (count($data) == 2) {
				$date = str_replace('.', '/', $data[1]);
				if (validateDate($date)) {
					$res[] = [
						'label' => trim($data[0]),
						'date'  => str_replace('.', '/', $data[1])
					];
				}
			}
		}
	}

	return $res;
}

function validateDate($date, $delimiter = '/') {
	$date_array = explode($delimiter, $date);

	if (count($date_array) === 3) {
		return checkdate($date_array[0], $date_array[1], $date_array[2]);
	} else {
		return false;
	}
}

function insertIfNotExist(mysqli $con, $value, $table = 'tags', $column = 'tag_name' ) {
	$unique_sql = "INSERT IGNORE INTO $table ($column) VALUES ('$value')";
	$con->query($unique_sql);
	$id = $con->insert_id;
	if ($id === 0) {
		return $con->query("SELECT * FROM $table WHERE tag_name='$value' LIMIT 1")->fetch_row()[0];
	}

	return $id;
}

function is_donor_tag( $field_name ) {
	global $con;
	
	if (isset($_SESSION['donor_tag_words'][ $field_name ]) && ($_SESSION['donor_tag_words'][ $field_name ] != "")) {
		//print "load session donor tag words...<br>";
		$tag_id = $_SESSION['donor_tag_words'][ $field_name ];
		//print "tag-id : <b>$tag_id</b> <br> donor tags list = <pre> " . print_r( $_SESSION['donor_tag_words'], true ) . "</pre>";
		return $tag_id;
	} else {
		//print "load donor tags from db...<br>";
		$donor_tags_sql = "select * from tags";
		$donor_tags_result = $con->query( $donor_tags_sql );
		
		$donor_tags = array();
		while( $tag = $donor_tags_result->fetch_array() ) {
			$donor_tags[ $tag['id'] ] = strtolower( $tag['tag_name'] );
			$donor_tag_words[ strtolower( $tag['tag_name'] ) ] = $tag['id'];
		}
		
		$_SESSION['donor_tags'] = $donor_tags;
		$_SESSION['donor_tag_words'] = $donor_tag_words;
		
		$tag_id = false;
		if (isset( $_SESSION['donor_tag_words'][ $field_name ] )) {
			$tag_id = $_SESSION['donor_tag_words'][ $field_name ];
			return $tag_id;
		} else {
			foreach( $_SESSION['donor_tags'] as $k => $d ) {
				if ($field_name == $d) {
					$tag_id = $k;
				}
			}
			return $tag_id;
		}
	
	}
	
}
?>