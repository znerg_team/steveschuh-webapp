-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2016 at 12:39 PM
-- Server version: 5.5.50
-- PHP Version: 5.5.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stevedata`
--

-- --------------------------------------------------------

--
-- Table structure for table `donors`
--

CREATE TABLE IF NOT EXISTS `donors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `business_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cellphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `donor_type` enum('company','individual') COLLATE utf8_unicode_ci NOT NULL,
  `employer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `donors`
--

INSERT INTO `donors` (`id`, `first_name`, `last_name`, `address1`, `address2`, `city`, `state`, `country`, `zip`, `phone1`, `phone2`, `home_phone`, `business_phone`, `cellphone`, `other_phone`, `email`, `email2`, `donor_type`, `employer_name`, `occupation`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Joh', 'errrr', 'Address1', 'Address2', 'Washington', 'AL', '', '6000', 'num1', 'num2', '', '', '', '', 'myemail@gmail.com', '', 'individual', 'employer', 'occupation', NULL, '2016-01-10 05:00:00', '2016-07-20 16:37:40'),
(2, 'Bonnie', 'Aardrup', '20 S Charles St., Ste. 300', '', 'Baltimore', 'MD', 'Balt. City', '21201', '', '', '', '', '', '', '', '\rElaine', 'individual', 'employer', 'occupation', NULL, '2016-04-10 04:00:00', '2016-07-20 16:14:59'),
(3, 'firstname', 'lastname', 'address1', 'address2', 'city', 'state', 'county', 'zip', 'phonenumber1', 'phonenumber2', 'homephone', 'businessphone', 'cellhone', '', 'email', 'alternateemail', '', 'employer', 'occupation', NULL, '2016-07-20 16:00:13', '2016-07-20 16:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `donor_donations`
--

CREATE TABLE IF NOT EXISTS `donor_donations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `donor_id` int(10) unsigned NOT NULL,
  `donation_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `donation_amount` double(8,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `donor_donations_donor_id_foreign` (`donor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `donor_tags`
--

CREATE TABLE IF NOT EXISTS `donor_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `donor_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `donor_tags_tag_id_donor_id_unique` (`tag_id`,`donor_id`),
  KEY `donor_tags_donor_id_foreign` (`donor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `donor_types`
--

CREATE TABLE IF NOT EXISTS `donor_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_19_152558_create_donors_table', 1),
('2016_06_19_152931_create_tags_table', 1),
('2016_06_19_153012_create_donor_tags_table', 1),
('2016_06_19_153051_create_donor_donations_table', 1),
('2016_07_19_070103_create_donor_types_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_tag_name_unique` (`tag_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(6, 'No Mail', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'No Call', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Endorsers', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'District Captain', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Lobbyist', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Clergy', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Schuh Family', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Rep. Club', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Financial', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Business Leader', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'AA Special', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Developers', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Political Activist', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'House of Delegates', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Senate', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Court/Other', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'AA Rep Central Committee', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'County Executive Office', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'AA County Council', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Annapolis Central Committee', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Annapolis City Council', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Federal', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Press', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Schuh Team Member', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Christmas Cards', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Yard Sign 2018', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Yard/Window Sign', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Large Yard Sign - 2014 Cycle', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Medium Yard Sign - 2014 Cycle', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Yard Sign Business Location', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '2014 Cycle Host/CoHost', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Event 1 (2018 Cycle)', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Event 2 (2018 Cycle)', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `user_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Denver', 'blah@blah.com', '$2y$10$vuOieBcRxNIDZBDZxg7K7eUCd/NC7qJjaD8A.cdn2XznaGG5B.nm6', '', NULL, '2016-07-20 10:19:14', '2016-07-20 10:19:14'),
(2, 'Mark', 'Sly', 'mark@mojo.biz', '$2y$10$LtLctScfzJKLh1oJ1xtNC.ss6MIPF.B41b55xh5X1KYuCwdI0Esze', '', NULL, '2016-07-20 10:19:14', '2016-07-20 10:19:14');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `donor_donations`
--
ALTER TABLE `donor_donations`
  ADD CONSTRAINT `donor_donations_donor_id_foreign` FOREIGN KEY (`donor_id`) REFERENCES `donors` (`id`);

--
-- Constraints for table `donor_tags`
--
ALTER TABLE `donor_tags`
  ADD CONSTRAINT `donor_tags_donor_id_foreign` FOREIGN KEY (`donor_id`) REFERENCES `donors` (`id`),
  ADD CONSTRAINT `donor_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
