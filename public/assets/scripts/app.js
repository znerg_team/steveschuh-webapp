/**
 * Created by olivercastro on 20/07/2016.
 */
(function($){
    $.fn.mask = function(){
        /*Init*/
        this.each(function(ndx,els){
            var el = $(els).val();
            el = el.replace(/\-/g,'')
            var chars = el.split("");
            var formattedText = '';
            for(var ndx = 1; ndx <= chars.length;ndx++){
                formattedText+= chars[ndx - 1];
                if(ndx % 3 == 0){
                    formattedText+= "-";
                }
            }
            $(els).val(formattedText);
        });

        this.on('keyup',function(evt){
            var el = $(this).val();
            el = el.replace(/\-/g,'')
            var chars = el.split("");
            var formattedText = '';
            for(var ndx = 1; ndx <= chars.length;ndx++){
                formattedText+= chars[ndx - 1];
                if(ndx % 3 == 0){
                    formattedText+= "-";
                }
            }
            $(this).val(formattedText);
        });
        this.on('change',function(evt){
            var el = $(this).val();
            el = el.replace(/\-/g,'')
            var chars = el.split("");
            var formattedText = '';
            for(var ndx = 1; ndx <= chars.length;ndx++){
                formattedText+= chars[ndx - 1];
                if(ndx % 3 == 0){
                    formattedText+= "-";
                }
            }
            $(this).val(formattedText);
        });
        this.on('keypress',function(evt){
            if(evt.which < 48 || evt.which >57)
                evt.preventDefault();
        });
    }
}(jQuery));
