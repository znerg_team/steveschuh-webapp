@extends('layouts.admin')

@section('page_heading', 'Advanced Search')

@section('section')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <form action="#" method="post" id="advance_form">
                    <div class="panel-heading">
                        <div class="input-group">
                           <span class="input-group-btn">
                               <button class="btn btn-default" type="button">
                                   <i class="fa fa-search fa-fw" aria-hidden="true"></i>
                               </button>
                           </span>
                            <input type="text" placeholder="Keyword" id="keyword" class="form-control" name="query">
                           <span class="input-group-btn">
                               <button class="btn btn-default">
                                   Find
                               </button>
                           </span>
                        </div>

                    </div>
                    <div class="panel-body" id="a-criteria">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="col-lg-4" style="padding-left:0">
                                        <label for="">Zip Code :</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="text" id="zip_code" class="form-control" name="zip_code" style="margin-top: -7px;"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <br /><br />
                                    <div class="col-lg-4" style="padding-left:0">
                                        <label for="">Contribution Period</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="period" name="period" value="" style="margin-top: -7px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <br /><br />
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="padding-top:10px;">Single Contribution Amount:</label>
                                        </div>
                                        <div class="col-md-3">
                                            <b>Min:</b> $<input type="text" name="donation_min" value="0" class="form-control" />
                                        </div>
                                        <div class="col-md-3">
                                            <b>Max:</b> $<input type="text" name="donation_max" value="50000" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Has Tags:</label>
                                    <select name="has_tags[]" class="donor-tags form-control" multiple="multiple"></select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="padding-top:10px;">County:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" name="county" value="" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="padding-top:15px;">Lifetime Contributions:</label>
                                        </div>
                                        <div class="col-md-3">
                                            <b>Min:</b> $<input type="text" name="donation_total_min" value="0" class="form-control" />
                                        </div>
                                        <div class="col-md-3">
                                            <b>Max:</b> $<input type="text" name="donation_total_max" value="50000" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="padding-top:15px;"># of Contributions:</label>
                                        </div>
                                        <div class="col-md-3">
                                            <b>Min:</b> <input type="text" name="donation_times_min" value="0" class="form-control" />
                                        </div>
                                        <div class="col-md-3">
                                            <b>Max:</b> <input type="text" name="donation_times_max" value="50" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Does NOT Have Tags:</label>
                                    <select name="does_not_have_tags[]" class="donor-tags form-control" multiple="multiple"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row tags-hide apply-tags" style="margin-bottom: 15px;">
        <div class="col-lg-12">
            <label>Donor Tags</label>
            <select class="donor-tags form-control" multiple="multiple"></select>
        </div>
    </div>
    <div class="row tags-hide apply-tags" style="margin-bottom: 15px; ">
        <div class="col-lg-12">
            <button class="btn btn-default pull" type="button" id="add-tag">Apply Tags!</button>
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="export-sql"/>
        <div class="col-lg-12">
            <button class="btn btn-info pull-right" id="export_search">
                Export CSV
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div id="result-count">
                <label>Matching Contributors:</label> <span>0</span>
            </div>
            <table class="table table-response table-striped" id="advance-search-result">
                <thead>
                <th><input type="checkbox" id="chkAll"></th>
                <th>Name</th>
                <th>Employer</th>
                <th>Total Contributions</th>
                <th>Last Contribution</th>
                <th># of Contributions</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($donors as $_donor)
                    <tr>
                        <td><input type="checkbox" name="multiaction" value="{{ $_donor->id }}" class="chk-action"></td>
                        <td>{{ $_donor->first_name . ' ' . $_donor->last_name }}</td>
                        <td>{{ $_donor->employer_name }}</td>
                        <td>$ {{ money_format('%i',$_donor->getTotalDonations()) }}</td>
                        <td>
                            @if($_donor->getLastDonated())
                                {{ \Carbon\Carbon::parse($_donor->getLastDonated()->donation_date)->format('M d, Y (D)') }}
                            @else
                                N/A
                            @endif
                        </td>
                        <td>{{ $_donor->getNumberOfDonations() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-right" id="pagination-container">
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".donor-tags").select2({
                data: {{ $_tags }},
                createSearchChoice : function(term){
                    return false;
                }
            });
            var useAdvance = false;
            $("#a-criteria").on('show.bs.collapse',function(){
                useAdvance = true;
            }.bind(useAdvance));
            $("#a-criteria").on('hide.bs.collapse',function(){
                useAdvance = false;
            }.bind(useAdvance));
            /*
            $('#donation_max_range').bootstrapSlider({
                value: [0,{{ $max }}],
                max: {{ $max }},
                min: {{ $min }}
            });
            $('#donation_total_range').bootstrapSlider({
                value: [0,{{ $max }}],
                max: {{ $max }},
                min: {{ $min }}
            });
            $('#donation_times_range').bootstrapSlider({
                value: [0,{{ $max }}],
                max: {{ $max }},
                min: {{ $min }}
            });
            */
            $('#period').daterangepicker({
                autoUpdateInput: false
            });

            $('#period').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('#period').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
            $('#advance_form').on('submit',function(){
                doSearchAndPaginate(1);
                return false;
            });
            $("#export_search").on('click',function(){
                if($('#export-sql').val() != ""){
                    $.ajax('{{ route('export_advanced_result') }}',{
                        headers:{
                            'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                        },
                        method : 'post',
                        data : {query : $('#export-sql').val()},
                        success : function(response){
                            window.location = response.path;
                        }
                    });
                }
            });
            $("#chkAll").on('click',function(){
                $('.chk-action').each(function(){
                    $(this).prop('checked',$("#chkAll").is(":checked"));
                });
            });
            $("#add-tag").on('click',function(){
                var hasChecked = false;

                $('.chk-action').each(function(ndx,el){
                    if(!hasChecked){
                        hasChecked = $(el).is(":checked");
                    }

                }.bind(hasChecked));

                if(hasChecked){
                    var donors = [];
                    $('.chk-action').each(function(ndx,el){
                        if($(el).is(":checked")){
                            donors.push($(el).val());
                        }
                    }.bind(donors));

                    $.ajax("{{ route('mass_update') }}",{
                        headers:{
                            'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                        },
                        method: 'post',
                        data :{ 'tags' : $('.donor-tags').val() ,'donors' :donors}
                    });
                }else{
                    alert("Please select entry");
                }

            });

            $("#pagination-container").on('click','a',function(){
                var ul = $(this).parent().parent();
                $(ul).children().each(function(ndx,el){
                    $(el).removeClass('active');
                });
                $(this).parent().addClass('active');
                var page = $(this).data('row')
                doSearchAndPaginate(page);
            });

            function doSearchAndPaginate(page){
                var data = $('#advance_form').serializeArray();
                data.push({name : 'useadvance', value : useAdvance});
                data.push({name : 'page', value: page});
                $.ajax('{{ route('post_advanced_search') }}',{
                    headers : {
                        'X-CSRF-TOKEN' : '{{ csrf_token()}}'
                    },
                    method : 'post',
                    data : data,
                    success: function(response){
                        if(response._dom ==""){
                            $('.apply-tags').addClass('tags-hide');
                        }else{
                            $('.apply-tags').removeClass('tags-hide');
                            $('.donor-tags').select2().trigger('change');
                        }
                        $('#export-sql').val(response.sql);
                        $('#advance-search-result tbody').empty();
                        $('#advance-search-result tbody').append(response._dom);
                        $('#pagination-container').empty();
                        $('#pagination-container').append(response.pagination);
                        $('#result-count span').html(response.count);
                    }
                });
            }
        });
    </script>
    <style>
        div.slider.slider-horizontal{
            width:170px;
        }
        div.tags-hide{
            display:none;
        }
    </style>
@endsection