@extends('layouts.admin')

@section('page_heading', 'Manage Account')

@section('section')
    <div class="row">
        <div class="col-md-12">
            @if($code == 1)
                <div class="alert alert-success" role="alert">
                    Account Information Updated.
                </div>
            @endif
            @if($code == 2)
                <div class="alert alert-warning" role="alert">
                    {{ $msg }}
                </div>
            @endif
            @if($code == 3)
                <div class="alert alert-danger" role="alert">
                    {{ $msg }}
                </div>
            @endif
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::model($_user,['url' => route('account'), 'role' => 'form', 'id' => 'account_form']) }}
            <input type="hidden" value="{{ $_user->id }}" id="user_id">
            <input type="hidden" value="{{ $_user->password }}" id="user_pw">
            <div class="form-group">
                <label>First Name</label>
                {{ Form::text('first_name',$_user->first_name,['class'  => 'form-control','placeholder' => 'First Name']) }}
            </div>
            <div class="form-group">
                <label>Last Name</label>
                {{ Form::text('last_name',$_user->last_name,['class'  => 'form-control','placeholder' => 'Last Name']) }}
            </div>
            <div class="form-group">
                <label>Email</label>
                {{ Form::text('email',$_user->email,['class'  => 'form-control','placeholder' => 'Email']) }}
            </div>
            <div class="form-group">
                <button class="btn btn-default" data-toggle="collapse" href="#newpwblock">
                    Change Password
                </button>

                <button class="btn btn-info pull-right">
                    Save
                </button>
            </div>

            <div class="collapse" id="newpwblock">
                <div class="form-group">
                    <label for="">Old Password</label>
                    <input type="password" class="form-control" placeholder="Old Password" name="old_password">
                </div>
                <div class="form-group">
                    <label for="">New Password</label>
                    <input type="password" placeholder="New Password" class="form-control" name="new_password">
                </div>
                <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input type="password" placeholder="Confirm Password" class="form-control" name="cnew_password">
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection