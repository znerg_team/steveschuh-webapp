@extends('layouts.admin')

@section('page_heading', 'Contributors')

@section('section')
    <div class="row">
        <div class="col-lg-10">
            <div class="input-group">
                <input type="text" class="form-control" id="donor_key">
            <span class="input-group-btn">
                <button class="btn btn-default" id="donor_search">Search</button>
            </span>
            </div>
        </div>
        <div class="col-lg-2">
            @if ($user->edit_rights)
            <a href="{{ url('admin/donors/add') }}">
                <button class="btn btn-default pull-right">
                    Add Contributor
                </button>
            </a>
            @endif
        </div>
    </div>
    <table class="table table-striped" id="donors">
        <thead>
        <tr>
            <th>Contributor Name</th>
            <th>State</th>
            <th>Zip Code</th>
            <th>Business Phone</th>
            <th>Email</th>
            <th>Total Contributions</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($donors as $_donor)
            <tr>
                <td>
                    @if (strlen(trim($_donor->first_name)) > 0)
                        {{ $_donor->last_name }}, {{ $_donor->first_name }}
                    @else
                        {{ $_donor->last_name }}
                    @endif
                </td>
                <td>{{ $_donor->state }}</td>
                <td>{{ $_donor->zip }}</td>
                <td>{{ $_donor->business_phone }}</td>
                <td>{{ $_donor->email }}</td>
                <td>{{ money_format('%.2n',$_donor->getTotalDonations()) }}</td>
                <td class="actions">
                    @if ($user->edit_rights)
                      <a href="{{ url('/admin/donors/edit') }}/{{ $_donor->id }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    @endif
                    <a href="{{ url('/admin/donors/view') }}/{{ $_donor->id }}"><i class="fa fa-binoculars" aria-hidden="true"></i></a>
                    @if ($user->edit_rights)
                      <a href="{{ url('/admin/donors/delete') }}/{{ $_donor->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    @endif
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-right" id="pagination-container">

            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            doSearchAndPaginate(1);
            $('#donor_search').click(function(){
                doSearchAndPaginate(1);
            });
            $("#pagination-container").on('click','a',function(){
                var ul = $(this).parent().parent();
                $(ul).children().each(function(ndx,el){
                    $(el).removeClass('active');
                });
                $(this).parent().addClass('active');
                var page = $(this).data('row')
                doSearchAndPaginate(page);
            });
            function doSearchAndPaginate(page){
                var keyword = $('#donor_key').val();
                $.ajax('{{ url('/admin/donors/find') }}',{
                    headers:{
                        'X-CSRF-TOKEN'  : '{{ csrf_token() }}'
                    },
                    method: 'post',
                    data: { 'keyword' : keyword, 'page' : page},
                    success: function(data, status){
                        $('#donors tbody').empty();
                        $('#donors tbody').append(data._dom);
                        $('#pagination-container').empty();
                        $('#pagination-container').append(data.pagination);
                    }
                });
            }
        });
    </script>
    <style>
        .actions a{
            margin-right:10px;
        }
        .actions a:last-child{
            margin-right:0;
        }
    </style>
@endsection
