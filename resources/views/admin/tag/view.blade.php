@extends('layouts.admin')

@section('page_heading', 'Tags')

@section('section')
    <div class="row">
        <div class="col-lg-12">
            <div class="input-group">
                <input type="text" placeholder="Find or add Tag" class="form-control" id="name_field">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="find_tag">Search</button>
                    <button class="btn btn-default" type="button" id="add_tag">Add</button>
                </span>
            </div>
        </div>
    </div>
    <table class="table table-striped" id="tags">
        <thead>
            <tr>
                <th>Tag Name</th>
                <th>Date Created</th>
                <th>Last Modified</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tags as $_tag)
                <tr>
                    <td>{{ $_tag->tag_name }}</td>
                    <td>{{ Carbon\Carbon::parse($_tag->created_at)->format('M d, Y (D)') }}</td>
                    <td>{{ Carbon\Carbon::parse($_tag->updated_at)->format('M d, Y (D)') }}</td>
                    <td class="actions">
                        <a href="{{ url('/admin/tags/edit/') }}/{{ $_tag->id }}"><i class="fa fa-pencil-square-o"></i></a>
                        <a href="{{ url('/admin/tags/delete/') }}/{{ $_tag->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach    
        </tbody>
    </table>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#add_tag').click(function(){
                var name = $('#name_field').val();
                if(name.trim() != ""){
                    $.ajax('{{ url('/admin/tags/add') }}',{
                        headers : {
                            'X-CSRF-TOKEN' : '{{ csrf_token()}}'
                        },
                        method: 'post',
                        data: { 'tagname' : name},
                        success: function(data, status){
                            $("#tags tbody").append(data._dom);
                        }
                    });
                }
            });
            $('#find_tag').click(function(){
                var keyword = $('#name_field').val();
                $.ajax('{{ url('/admin/tags/find') }}',{
                    headers: {
                        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                    },
                    method : 'post',
                    data: { 'keyword' : keyword},
                    success: function(data, status){
                        $('#tags tbody').empty();
                        $('#tags tbody').append(data._dom);
                    }
                })
            });
        });
    </script>
    <style>
        .actions a{
            margin-right:10px;
        }
        .actions a:last-child{
            margin-right:0;
        }
    </style>
@endsection