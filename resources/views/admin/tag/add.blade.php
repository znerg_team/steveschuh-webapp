@extends('layouts.admin')

@section('page_heading', 'Edit Tag')

@section('section')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ url('/admin/tags/edit/') }}/{{ $_tag->id }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>Tag name</label>
                    <input type="text" name="tagname" class="form-control" value="{{ $_tag->tag_name }}">
                </div>
                <div class="form-group">
                    <button class="btn btn-default">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection