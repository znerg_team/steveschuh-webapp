
@extends('layouts.admin')

@section('page_heading', 'Contributor ID:' . $_donor->id)
@section('section')
    <div class="row">
        <div class="col-lg-6">
            <input type="hidden" value="{{ $_donor->id }}" id="donor_owner">
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input class="form-control" type="text" disabled="disabled" value="{{ $_donor->first_name }}">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name/Company Name</label>
                <input class="form-control" value="{{ $_donor->last_name}}" type="text" disabled="disabled">
            </div>
            <div class="form-group">
                <label for="last_name">Address</label>
                <input class="form-control" value="{{ $_donor->address1 }}" type="text" disabled="disabled">
            </div>
            <div class="form-group">
                <input class="form-control" value="{{ $_donor->address2 }}" type="text" disabled="disabled">
            </div>
            <div class="form-group">
                <div class="col-lg-3" style="padding-left:0">
                    <label for="city">City</label>
                    <input class="form-control" value="{{ $_donor->city }}" type="text" disabled="disabled">

                </div>
                <div class="col-lg-3">
                    <label for="state">State</label>
                    <input type="text" class="form-control" value="{{ App\Helpers\Data::getStateName($_donor->state) }}" disabled="disabled">
                </div>
                <div class="col-lg-3">
                    <label for="zip">Zip Code</label>
                    <input class="form-control" value="{{ $_donor->zip }}" type="text" disabled="disabled">
                </div>
                <div class="col-lg-3">
                    <label for="county">County</label>
                    <input type="text" class="form-control" value="{{ $_donor->country }}" disabled="disabled">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-lg-6" style="padding-left: 0">
                    <label for="email">Email Address</label>
                    <input class="form-control" value="{{ $_donor->email }}" type="email" disabled="disabled">
                </div>
                <div class="col-lg-6">
                    <label for="email">Alternate Email Address</label>
                    <input class="form-control" value="{{ $_donor->email2 }}" type="email" disabled="disabled">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-6" style="padding-left: 0">
                    <label>Occupation</label>
                    {{ Form::text('occupation',$_donor->occupation,['class' => 'form-control', 'placeholder' => 'Occupation','disabled' => 'disabled']) }}
                </div>
                <div class="col-lg-6">
                    <label>Employer</label>
                    {{ Form::text('employer_name', $_donor->employer_name,['class' => 'form-control', 'placeholder' => 'Employer Name','disabled' => 'disabled']) }}
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Date Created</label>
                <input class="form-control" value="{{ Carbon\Carbon::parse($_donor->created_at)->format('m-d-Y') }}" type="text" disabled="disabled">
            </div>
            <div class="form-group">
                <label>Contributor Type</label>
                {{ Form::select('donor_type', App\Helpers\Data::getDonorTypes(),$_donor->donor_type,['class' => 'form-control','disabled' => 'disabled'])}}
            </div>
            <div class="form-group">
                <label>Home Phone</label>
                {{ Form::text('home_phone',App\Helpers\Data::formatPhone($_donor->home_phone),['class' => 'form-control', 'placeholder' => 'Home Phone No.','disabled' => 'disabled']) }}
            </div>
            <div class="form-group">
                <label>Business Phone</label>
                {{ Form::text('business_phone',App\Helpers\Data::formatPhone($_donor->business_phone),['class' => 'form-control', 'placeholder' => 'Business Phone No.','disabled' => 'disabled']) }}
            </div>
            <div class="form-group">
                <label>Cellular Phone</label>
                {{ Form::text('cellphone',App\Helpers\Data::formatPhone($_donor->cellphone),['class' => 'form-control', 'placeholder' => 'Cellular Phone No.','disabled' => 'disabled']) }}
            </div>
            <div class="form-group">
                <label>Other Phone</label>
                {{ Form::text('cellphone',App\Helpers\Data::formatPhone($_donor->phone1),['class' => 'form-control', 'placeholder' => 'Other Phone','disabled' => 'disabled']) }}
            </div>
            <div class="form-group">
                <label>Notes</label>
                {{ Form::textarea('notes',$_donor->notes,['class' => 'form-control','placeholder' => 'Notes','rows' => 5,'disabled' => 'disabled']) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label>Contributor Tags</label>
                <select class="donor-tags form-control" multiple="multiple"></select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1>Total Contributions : $ <span id="total_amount">{{ money_format('%i',$_donor->getTotalDonations()) }}</span></h1>
                </div>
                <div class="col-lg-6">
                    <!--<button type="button" class="btn btn-default" style="margin-top:20px;" data-toggle="modal" data-target="#donation_modal">Add Contribution</button>-->
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-striped" id="donation_list">
                <tbody>
                    @foreach($_donor->donations as $donation)
                        <tr>
                            <td>$ {{ money_format('%i', $donation->donation_amount) }}</td>
                            <td>{{ Carbon\Carbon::parse($donation->donation_date)->format('M d, Y (D)') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="donation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Contribution</h4>
                </div>
                <div class="modal-body" id="donor_modal_body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Amount :</span>
                                <input type="text" class="form-control" placeholder="Amount" id="donation_amt">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Date : &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input type="text" placeholder="Date" id="donation_date" class="form-control"  style="z-index: 100000;">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="donation_add">Add Contribution</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#donation_date").datepicker();
            $(document).ready(function(){
                $('.phone_format').mask();
            });
            $('#add-tag').click(function(){
                var elems = $('.donor-tags').select2('data');
                var options = $.map(elems,function(val, i){ return { "id": val.id, "value" : val.text}});
                $.ajax('{{ url('admin/donors/tag/add') }}',{
                    headers : {
                        'X-CSRF-TOKEN' : '{{ csrf_token()}}'
                    },
                    method: 'post',
                    data : {"options" : options, 'owner' : $("#donor_owner").val()}
                });
            });

            $(".donor-tags").select2({
                data: {{ $_tags }},
                createSearchChoice : function(term){
                    return false;
                }
            });
            $('#donation_add').click(function(){
                var amt = $('#donation_amt').val();
                var donor = $('#donor_owner').val();
                var donation_date = $("#donation_date").val();
                if(amt.trim() !== '' && donation_date.trim() !== ''){
                    $.ajax('{{  route('post_donation') }}',{
                        headers :{
                            'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                        },
                        method : 'post',
                        data: {'amount' : amt, 'donor' : donor, 'date' : donation_date },
                        success: function(resp){
                            $('#donation_amt').val('');
                            reloadDonationTable();
                        }
                    });
                }
            });

            //Reload donations table
            function reloadDonationTable(){
                $.ajax('{{ route('fetch_donation') }}',{
                    headers :{
                        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                    },
                    method : 'post',
                    data : {'donor' : $('#donor_owner').val()},
                    success : function(response){
                        $("#total_amount").html(response._total);
                        $('#donation_list tbody').empty();
                        $('#donation_list tbody').append(response._dom);
                    }
                });
            }
            var data = {{ $_dntags }}
            var multi = $('.donor-tags').select2();
            multi.val({{ $_dntags }}).trigger("change");
            //Remove tags
            $('.donor-tags').on('select2:unselect',function(evt){
                var tagId = evt.params.data.id;
                var donorId = $("#donor_owner").val();
                updateDonorTags(tagId,donorId,'delete');

            });
            $('.donor-tags').on('select2:select',function(evt){
                var tagId = evt.params.data.id;
                var donorId = $("#donor_owner").val();
                updateDonorTags(tagId,donorId,'insert');
            });
            function updateDonorTags(tag, donor, type){
                $.ajax('{{ route('donor_tag_post'); }}',{
                    headers:{
                        'X-CSRF-TOKEN'  : '{{ csrf_token() }}'
                    },
                    data: { 'donor': donor, 'tag': tag, 'type': type },
                    method: 'post',
                    success: function(response){

                    }
                });
            }
        });
    </script>
    <style>
        #donor_modal_body .input-group{
            margin-bottom:10px;
        }
    </style>
@endsection
