
@extends('layouts.admin')

@section('page_heading', $form_type . ' Contributor')

@section('section')
    <?php
    $states = App\Helpers\Data::getUsStates();
    $countries = App\Helpers\Data::getCountries();
    ?>
    {{ Form::model($_donor,['url' => url($post_url), 'role'=>'form','id' => 'donor_form']) }}
    <div class="row">
        <div class="col-lg-6">
            @if(strtolower($form_type) != 'new')
                <input type="hidden" value="{{ $_donor->id }}" id="donor_owner">
            @endif
            <div class="form-group">
                <label for="first_name">First Name</label>
                {{ Form::text('first_name',$_donor->first_name,['class' => 'form-control','placeholder' => 'First Name']) }}
            </div>
            <div class="form-group">
                <label for="last_name">Last Name/Company Name</label>
                {{ Form::text('last_name',$_donor->last_name,['class' => 'form-control','placeholder' => 'Last Name']) }}
            </div>
            <div class="form-group">
                <label for="last_name">Address</label>
                {{ Form::text('address1',$_donor->address1,['class' => 'form-control','placeholder' => 'Address Line 1']) }}
            </div>
            <div class="form-group">
                {{ Form::text('address2',$_donor->address2,['class' => 'form-control','placeholder' => 'Address Line 2']) }}
            </div>
            <div class="form-group">
                <div class="col-lg-3" style="padding-left:0">
                    <label for="city">City</label>
                    {{ Form::text('city',$_donor->city,['class' => 'form-control', 'placeholder' => 'City']) }}
                </div>
                <div class="col-lg-3">
                    <label for="state">State</label>
                    {{ Form::select('state',$states,$_donor->state,['class' => 'form-control'])}}
                </div>
                <div class="col-lg-3">
                    <label for="zip">Zip Code</label>
                    {{ Form::text('zip', $_donor->zip,['class' => 'form-control', 'placeholder' => 'Zip','id' => 'zip-code']) }}
                </div>
                <div class="col-lg-3">
                    <label for="county">County</label>
                    {{ Form::text('country', $_donor->country,['class' => 'form-control', 'placeholder' => 'County', 'id' => 'county']) }}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-lg-6" style="padding-left: 0">
                    <label for="email">Email Address</label>
                    {{ Form::email('email',$_donor->email,['class' => 'form-control','placeholder' => 'Email Address']) }}
                </div>
                <div class="col-lg-6">
                    <label for="email2">Alternate Email Address</label>
                    {{ Form::email('email2',$_donor->email2,['class' => 'form-control','placeholder' => 'Alternate Email Address']) }}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-lg-6" style="padding-left:0;">
                    <label>Occupation</label>
                    {{ Form::text('occupation',$_donor->occupation,['class' => 'form-control', 'placeholder' => 'Occupation']) }}
                </div>
                <div class="col-lg-6">
                    <label>Employer</label>
                    {{ Form::text('employer_name', $_donor->employer_name,['class' => 'form-control', 'placeholder' => 'Employer Name']) }}
                </div>
            </div>
            @if(strtolower($form_type) != 'new')
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button"  class="btn btn-primary" style="margin: 20px 0" data-toggle="modal" data-target="#link_donor_modal">Link Contributor</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr><th>Linked Contributor Name</th><th></th></tr>
                                </thead>
                                <tbody id="link_donor_tbody"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-lg-6">
            @if(strtolower($form_type) != 'new')
                <div class="form-group">
                    <label>Date Created</label>
                    {{ Form::text('created_at',Carbon\Carbon::parse($_donor->created_at)->format('m-d-Y'),['class' => 'form-control', 'placeholder' => 'mm-dd-yyyy']) }}
                </div>
            @endif
            <div class="form-group">
                <label>Contributor Type</label>
                {{ Form::select('donor_type', App\Helpers\Data::getDonorTypes(),$_donor->donor_type,['class' => 'form-control'])}}
            </div>
            <!--
            <div class="form-group">
                {{ Form::text('phone2',$_donor->phone2,['class' => 'form-control phone_format', 'placeholder' => 'Phone No.']) }}
            </div>
            -->
            <div class="form-group">
                <label>Home Phone</label>
                {{ Form::text('home_phone',App\Helpers\Data::formatPhone($_donor->home_phone),['class' => 'form-control', 'placeholder' => 'Home Phone No.']) }}
            </div>
            <div class="form-group">
                <label>Business Phone</label>
                {{ Form::text('business_phone',App\Helpers\Data::formatPhone($_donor->business_phone),['class' => 'form-control', 'placeholder' => 'Business Phone No.']) }}
            </div>
            <div class="form-group">
                <label>Cellular Phone</label>
                {{ Form::text('cellphone',App\Helpers\Data::formatPhone($_donor->cellphone),['class' => 'form-control', 'placeholder' => 'Cellular Phone No.']) }}
            </div>
            <div class="form-group">
                <label>Other Phone</label>
                {{ Form::text('phone1',App\Helpers\Data::formatPhone($_donor->phone1),['class' => 'form-control', 'placeholder' => 'Phone No.']) }}
            </div>
            <div class="form-group">
                <label>Notes</label>
                {{ Form::textarea('notes',$_donor->notes,['class' => 'form-control','placeholder' => 'Notes','rows' => 5]) }}
            </div>
            <div class="form-group">
                <button class="btn btn-default pull-right" type="submit">
                    @if(strtolower($form_type) == 'new')
                        Add New Record
                    @else
                        Update Record
                    @endif
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    @if(strtolower($form_type) != 'new')
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Contributor Tags</label>
                    <select class="donor-tags form-control" multiple="multiple"></select>
                </div>

                <div class="form-group">
                    <button class="btn btn-default pull-right" id="add-tag">
                        Add/Update Tag
                    </button>
                </div>

            </div>
        </div>
    @endif
    @if(strtolower($form_type) != 'new')
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Total Contributions : $ <span id="total_amount">{{ money_format('%i',$_donor->getTotalDonations()) }}</span></h1>
                    </div>
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-default" style="margin-top:20px;" data-toggle="modal" data-target="#donation_modal">Add Contribution</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <table class="table table-striped" id="donation_list">
                    <tbody>
                    @foreach($_donor->donations as $donation)
                        <tr>
                            <td>$ {{ money_format('%i', $donation->donation_amount) }}</td>
                            <td>{{ Carbon\Carbon::parse($donation->donation_date)->format('M d, Y (D)') }}</td>
                            <td><a data-toggle="modal" data-target="#donation_modal" href="{{ url('/admin/donors/edit/contribution') }}/{{ $donation->id }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="donation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Contribution</h4>

                    </div>
                    <div class="modal-body" id="donor_modal_body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon">Amount :</span>
                                    <input type="text" class="form-control" placeholder="Amount" id="donation_amt">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon">Date : &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <input type="text" placeholder="Date" id="donation_date" class="form-control"  style="z-index: 100000;">
                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="donation_add">Add Contribution</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(strtolower($form_type) != 'new')
    <!-- Link donor record Modal -->
    <div class="modal fade" id="link_donor_modal" tabindex="-1" role="dialog" aria-labelledby="link_donor_modal">
        <div class="modal-dialog  modal-sm" role="document">
            <form id="link_donor_form" class="modal-content" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="link_donor_modal">Link Donor Record</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="link_donor">Donor</label>
                        <input class="form-control" id="link_donor" type="text" data-provide="typeahead" placeholder="
Please enter at least 3 letters">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Add">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
    @endif
    <script type="text/javascript">
        $(document).ready(function(){
        <?php if(strtolower($form_type) != 'new') { ?>
            var input = $("#link_donor");
            var form  = $('#link_donor_form');
            var tbody = $('#link_donor_tbody');
            var modal = $('#link_donor_modal');

            $.get('<?php echo route('get_link_donor', ['id' => $_donor->id]) ?>', function (res) {
                $.each(res, function (key, res) {
                    addItem(res);
                });
            });

            tbody.on('click', '.link_donor_remove',function(event) {
                var result = confirm("Want to delete?");
                if (result) {
                    var tr = $(this).closest('tr')
                    var id = tr.data('id');

                    $.ajax('<?php echo route('add_link_donor', ['id' => $_donor->id]) ?>',{
                        headers :{
                            'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                        },
                        method : 'delete',
                        data: {'id': id },
                        success: function(){
                            tr.remove();
                        },
                        error: function () {
                            alert('error');
                        }
                    });
                }
                event.preventDefault();
            });

            form.submit(function( event ) {
                var current = input.typeahead("getActive");
                if (current) {
                    if (current.first_name+" "+current.last_name == input.val()) {
                        var button = form.find('input[type="submit"]');
                        form.find('input[type="submit"]').prop('disabled', true);
                        $.ajax('<?php echo route('add_link_donor', ['id' => $_donor->id]) ?>',{
                            headers :{
                                'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                            },
                            method : 'post',
                            data: {'id': current.id },
                            success: function(resp){
                                addItem(resp);
                                button.prop('disabled', false);
                                modal.modal('hide');
                                input.val('');
                            },
                            error: function () {
                                button.prop('disabled', false);
                            }
                        });
                    }
                }
                event.preventDefault();
            });

            input.typeahead({ source: function (query, proccess) {
                return $.get('/admin/donors/find?keyword='+query, function (res) {
                    return proccess(res);
                });
            }, autoSelect: true, displayText: function(item){ return item.first_name + " " + item.last_name;}});

            function addItem(item) {
                var full_name  = item.first_name + ' ' + item.last_name;
                var name       = $('<td></td>').html('<a href="/admin/donors/edit/' + item.id + '"" class="">' + full_name + '</a>');
                var remove     = $('<td></td>').html('<a href="#" class="link_donor_remove"><i class="fa fa-trash""></i></a>');
                var tr         = $('<tr></tr>').append(name).append(remove);

                tr.data('id', item.id);
                tbody.append(tr);
            }
        <?php } ?>


            function doSearchCounty(){
                $.ajax('{{ route('post_county') }}',{
                    headers : {
                        'X-CSRF-TOKEN' : '{{ csrf_token()}}'
                    },
                    method: 'post',
                    data: { zip : $('#zip-code').val()},
                    success :function(response){
                        $('#county').val(response.county);
                    }
                })
            }
            $('#zip-code').on('change',function(){
                doSearchCounty();
            });
            $('#zip-code').on('keypress',function(){
                doSearchCounty();
            });
            $('#zip-code').on('keyup',function(){
                doSearchCounty();
            });
            $(".donor-tags").select2({
                data: {{ $_tags }},
                createSearchChoice : function(term){
                    return false;
                }
            });
            $(document).ready(function(){
                $('.phone_format').mask();
            });
            $("#donation_date").datepicker();
            @if(strtolower($form_type) != 'new')
                $('#add-tag').click(function(){
                var elems = $('.donor-tags').select2('data');
                var options = $.map(elems,function(val, i){ return { "id": val.id, "value" : val.text}});
                $.ajax('{{ url('admin/donors/tag/add') }}',{
                    headers : {
                        'X-CSRF-TOKEN' : '{{ csrf_token()}}'
                    },
                    method: 'post',
                    data : {"options" : options, 'owner' : $("#donor_owner").val()},
                    success: function (response){
                        alert("Tag entries successfully added/updated");
                    }
                });
            });

            var data = {{ $_dntags }}
            var multi = $('.donor-tags').select2();
            multi.val({{ $_dntags }}).trigger("change");

            $('#donation_add').click(function(){
                var amt = $('#donation_amt').val();
                var donor = $('#donor_owner').val();
                var donation_date = $("#donation_date").val();
                if(amt.trim() !== '' && donation_date.trim() !== ''){
                    $.ajax('{{  route('post_donation') }}',{
                        headers :{
                            'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                        },
                        method : 'post',
                        data: {'amount' : amt, 'donor' : donor, 'date' : donation_date },
                        success: function(resp){
                            $('#donation_amt').val('');
                            reloadDonationTable();
                        }
                    });
                }
            });

            $(document).on('click','#donation_edit', function(){
                var amt = $('#donation_amt').val();
                var donation_date = $("#donation_date").val();
                if(amt.trim() !== '' && donation_date.trim() !== ''){
                    $.ajax($('#edit-contribution-form').attr('action'),{
                        headers :{
                            'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                        },
                        method : 'post',
                        data: {'amount' : amt, 'date' : donation_date },
                        success: function(resp){
                            $('#donation_amt').val('');
                            reloadDonationTable();
                        }
                    });
                }
            });

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });

            //Reload donations table
            function reloadDonationTable(){
                $.ajax('{{ route('fetch_donation') }}',{
                    headers :{
                        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                    },
                    method : 'post',
                    data : {'donor' : $('#donor_owner').val()},
                    success : function(response){
                        $('#total_amount').html(response._total);
                        $('#donation_list tbody').empty();
                        $('#donation_list tbody').append(response._dom);
                    }
                });
            }
            @else
                $('#donor_form').on('submit',function(){
                var form_data = $('#donor_form').serializeArray();
                $.ajax('{{ route('insert_donor') }}',{
                    headers : {
                        'X-CSRF-TOKEN' : '{{ csrf_token()}}'
                    },
                    method: 'post',
                    data: form_data,
                    success: function(response){
                        window.location = response._redirect;
                    }
                });
                return false;
            });
            @endif
        });
    </script>
    <style>
        #donor_modal_body .input-group{
            margin-bottom:10px;
        }
    </style>
@endsection
