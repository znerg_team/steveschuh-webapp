@extends('layouts.admin')

@section('page_heading', 'Import Donors')

@section('section')
    <div class="row">
        <div class="col-md-12">
            @if(!empty($response))
                <div class="alert alert-success" role="alert">
                    {{ $response['newrecords'] }} were added.
                    {{ $response['updatedrecords'] }} were updated.
                </div>
            @endif
        </div>

    </div>
    <form action="{{ route('donor_import') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-lg-6">
                <input type="file" name="csv_file" class="filestyle" data-icon="false">
            </div>
        </div>
        <div class="row" style="margin-top:10px">
            <div class="col-lg-6">
                <button type="submit" class="btn btn-default pull-right">
                    Upload
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p><a href="{{ asset('samples/sample_import.csv') }}">Download sample .csv</a></p>
            </div>
        </div>
    </form>
@endsection