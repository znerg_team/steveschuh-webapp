                    {{ Form::open(array('url' => '/admin/donors/edit/contribution/'.$id, 'id' => 'edit-contribution-form')) }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Contribution</h4>

                    </div>
                    <div class="modal-body" id="donor_modal_body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon">Amount :</span>
                                    <input type="text" class="form-control" placeholder="Amount" id="donation_amt" value="{{ $amount }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon">Date : &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <input type="text" placeholder="Date" id="donation_date" class="form-control"  style="z-index: 100000;"  value="{{ $donation_date }}">
                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
                        &nbsp; &nbsp; &nbsp;-->
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="donation_edit">Edit Contribution</button>
                    </div>
                    {{ Form::close() }}