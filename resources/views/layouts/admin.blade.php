@extends('layouts.plane')
@section('body')
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url ('') }}">Contributor Data</a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="#">
                        <i class="fa fa-user fa-fw"></i> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                    </a>
                </li>
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li {{ Request::is('*admin/donors/*') ? 'class="active"' : ''}}>
                            <a href="{{ url ('/admin/donors') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Contributors</a>
                        </li>
                        <li {{ Request::is('*admin/import/*')  ? 'class="active"' : ''}}>
                            <a href="{{ url('/admin/import/donors') }}"><i class="fa fa-file fa-fw"></i> Import Contributors (CSV)</a>
                        </li>
                        <li {{ Request::is('*admin/tags/*') ? 'class="active"' : '' }}>
                            <a href="{{ url('/admin/tags') }}"><i class="fa fa-tags fa-fw"></i> Manage Tags</a>
                        </li>
                        <li {{ Request::is('*admin/advanced-search') ? 'class="active"' : '' }}>
                            <a href="{{ url('/admin/advanced-search') }}"><i class="fa fa-search fa-fw"></i> Advanced Search</a>
                        </li>
                        <li {{ Request::is('*admin/account') ? 'class="active"' : '' }}>
                            <a href="{{ url('/admin/account') }}"><i class="fa fa-cogs fa-fw"></i> My Account</a>
                        </li>
                        <li>
                            <a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
            </div>

            @yield('section')

        </div>
    </div>
@stop