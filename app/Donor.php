<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model {

    protected $dates = ['deleted_at'];

    public function donations()
    {
        return $this->hasMany('App\DonorDonation');
    }
    public function tags(){
        return $this->hasMany('App\DonorTag');
    }
    public function getNumberOfDonations(){
        return count($this->donations);
    }
    public function getLastDonated(){
        return $this->donations->last();
    }
    public function getTotalDonations(){
        $total = 0;
        foreach ($this->donations as $_donation){
            $total+= $_donation->donation_amount;
        }
        return $total;
    }
}
