<?php
use Illuminate\Pagination\Paginator;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return redirect('/admin');
});

/* Refactored Application routes */

Route::group(['middleware' => 'auth'],function(){
	setlocale(LC_MONETARY, 'en_US.UTF-8');

	//Dashboard route
	Route::get('/admin',function(){

		return redirect('/admin/donors');
	});

	//Account Management
	Route::match(['get','post'],'admin/account/',['as'	=> 'account',function(){
	    //print Hash::make('temp123');
        //exit;

		$_user = Auth::user();
		$inputs = Input::all();
		$response = array(
			'_user' => '',
			'msg'	=> '',
			'code'	=> 0,
		);
		if(Request::isMethod('post')){
			$_user->first_name = $inputs['first_name'];
			$_user->last_name = $inputs['last_name'];
			if($_user->email !== $inputs['email']){
				$query = App\User::query()->where('email','=',$inputs['email']);
				if(count($query->get())==0){
					$_user->email = trim($inputs['email']);
					$_user->save();
					$response['code'] = 1;
				}else{
					$response['code'] = 3;
					$response['msg'] = 'Email address already in use';
				}
			}else{
				$_user->save();
				$response['code'] = 1;
			}

			if(!empty(trim($inputs['new_password'])) && !empty(trim($inputs['cnew_password']))){
				$isOldSame = Hash::check($inputs['old_password'], $_user->password);
				if($isOldSame){
					if(trim($inputs['new_password']) === trim($inputs['cnew_password'])){
						$_user->password = Hash::make($inputs['new_password']);
						$_user->save();
						$response['code'] = 1;
					}else{
						$response['code'] = 2;
						$response['msg'] = 'New password don\'t match';
					}
				}else{
					$response['code'] = 2;
					$response['msg'] = 'Incorrect current password';
				}
			}
		}
		$response['_user'] = $_user;
		return View::make('admin/account',$response);
	}]);

	//Import donor
	Route::match(['get','post'],'/admin/import/donors',['as' => 'donor_import',function(){
		$response = array();
		if(Request::isMethod('POST') && Request::hasFile('csv_file')){
			ini_set('auto_detect_line_endings',1);
			$newRecords = 0;
			$updatedRecords = 0;

			$_file = Request::file('csv_file');
			$original_filename = $_file->getClientOriginalName();
			$folder = date("Y-m-d") . DIRECTORY_SEPARATOR;
			Storage::put($folder . $original_filename ,file_get_contents($_file->getRealPath()));
			$store_path  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
			$file_obj = fopen($store_path . $folder . $original_filename,'r');

			while(!feof($file_obj)){
				$item = fgetcsv($file_obj,0,',');
				$phones = explode(';',$item[8]);
				$find = App\Donor::query();
				$email = $item[7];
				if(!empty($phones)){
					$find = $find->where(function($query) use($email, $phones) {
						$query->orWhereIn('phone1',$phones)
							->orWhereIn('phone2',$phones)
							->orWhere('email','=', $email);
					});
				}else{
					$find = $find->where('email','=',$item[7]);
				}
				$find = $find->get();
				$find = array();
				if(count($find) == 0){
					$newRecords++;
					$_donor = new App\Donor();
					$_donor->first_name = $item[0];
					$_donor->last_name = $item[1];
					$_donor->address1 = $item[2];
					$_donor->address2 = $item[3];
					$_donor->city = $item[4];
					$_donor->state = $item[5];
					$_donor->zip = $item[6];
					$_donor->email = empty($item[7]) ? null : '';


					$_donor->phone1 = empty(preg_replace('/-/','',$item[8])) ? null : preg_replace('/-/','',$item[8]);
					$_donor->phone2 = empty(preg_replace('/-/','',$item[9])) ? null : preg_replace('/-/','',$item[9]);

					$_donor->occupation = $item[11];
					$_donor->employer_name = $item[12];
                    if(count($item) >= 15){
                        $_donor->country = $item[14];
                    }
                    if(count($item) >= 16){
                        $_donor->donor_type = $item[15];
                    }
					if(count($item) >= 17){
						$_donor->home_phone = preg_replace('/-/','',$item[16]);
					}
					if(count($item) >= 18){
						$_donor->business_phone = preg_replace('/-/','',$item[17]);
					}
					if(count($item) >= 19){
						$_donor->cellphone = preg_replace('/-/','',$item[18]);
					}
                    if(count($item) >= 20){
                        $_donor->email2 = empty($item[19]) ? null : $item[19];
                    }
                    if(count($item) == 21){
                        $_donor->created_at = Carbon\Carbon::parse($item[20])->format('mm-d-y');
                    }
					$_donor->save();


					//Tags
					$tags = explode(';',$item[10]);
					foreach($tags as $tag){
						$_tag = App\Tag::where('tag_name','=',$tag)->get();
						$tagObj = null;
						if(count($_tag) == 0){
							$newTag = new App\Tag();
							$newTag->tag_name = $tag;
							$newTag->save();
							$tagObj = $newTag;
						}else{
							$tagObj = App\Tag::where('tag_name','=',$tag)->first();
						}
						$donorTag = new \App\DonorTag();
						$donorTag->donor_id = $_donor->id;
						$donorTag->tag_id = $tagObj->id;
						$donorTag->save();
					}
					//Donations
					if(count($item) == 14){
						$donations = explode(';',$item[13]);
						foreach($donations as $donation){
							$parts = explode(':', $donation);
							$timestamp = DateTime::createFromFormat('Y-m-d',$parts[1]);
							$ddate = date("Y-m-d H:m:s", $timestamp->getTimestamp());
							$_donations = new App\DonorDonation();
							$_donations->donor_id = $_donor->id;
							$_donations->donation_amount = $parts[0];
							$_donations->donation_date = $ddate;
							$_donations->save();
							//dd($_donations);
						}
					}

				}else{
					$updatedRecords++;
					//Update record
					$_donor = $find->first();
					$_donor->first_name = $item[0];
					$_donor->last_name = $item[1];
					$_donor->address1 = $item[2];
					$_donor->address2 = $item[3];
					$_donor->city = $item[4];
					$_donor->state = $item[5];
					$_donor->zip = $item[6];
					$_donor->email = empty($item[7]) ? null : $item[7];
					$_donor->phone1 = empty(preg_replace('/-/','',$item[8])) ? null : preg_replace('/-/','',$item[8]);
					$_donor->phone2 = empty(preg_replace('/-/','',$item[9])) ? null : preg_replace('/-/','',$item[9]);
                    if(count($item) >= 15){
                        $_donor->country = $item[14];
                    }
					if(count($item) >= 16){
						$_donor->donor_type = $item[15];
					}
					if(count($item) >= 17){
						$_donor->home_phone = preg_replace('/-/','',$item[16]);
					}
					if(count($item) >= 18){
						$_donor->business_phone = preg_replace('/-/','',$item[17]);
					}
					if(count($item) >= 19){
						$_donor->cellphone = preg_replace('/-/','',$item[18]);
					}
					if(count($item) >= 20){
						$_donor->email2 = empty($item[19]) ? null : $item[19];
					}
					if(count($item) == 21){
						$_donor->created_at = Carbon\Carbon::parse($item[20])->format('mm-d-y');
					}
					$_donor->save();
					//dd($find->first());
				}
			}
			$response['newrecords'] = $newRecords;
			$response['updatedrecords'] = $updatedRecords;
		}
		return View::make('admin/donor/import',array('response' => $response));
	}]);

	//List Donor
	Route::get('/admin/donors',function(){
		//$_donor = App\Donor::paginate(1);
		$_donor = array();
		//dd($_donor);
		return View::make('admin/donor',array('donors' => $_donor, 'user' => Auth::user()));
	});

	Route::get('/admin/donors/find', ['as' => 'find_donors', function(Illuminate\Http\Request $request) {
		$keyword = $request->get('keyword');
		if ($keyword && strlen($keyword) >= 3) {
			return \App\Donor::select('id', 'last_name', 'first_name')
				->where('first_name', 'like', '%'.$keyword.'%')
				->orWhere('last_name', 'like', '%'.$keyword.'%')
				->get()
				;
		} else {
			return [];
		}
	}]);

	//Find Donor
	Route::post('/admin/donors/find',function(){
		$user = Auth::user();

	    $per_page = 200;
		$keyword = Input::get('keyword','');
		$page = Input::get('page','1');
		$donors = DB::table('donors')
			->select(DB::raw('*,`donors`.`id` as `source`'))
			->leftJoin('donor_tags','donors.id', '=', 'donor_tags.donor_id')
			->leftJoin('tags','donor_tags.tag_id','=','tags.id')
			->groupBy('donors.id')
			->where(function($query) use ($keyword) {
                    return $query
										->orwhere('tags.tag_name','LIKE', '%' . $keyword . '%')
										->orWhere('donors.last_name', 'LIKE', '%' . $keyword . '%')
										->orWhere('donors.first_name', 'LIKE', '%' . $keyword . '%')
										->orWhere(DB::raw("CONCAT(first_name,last_name)"), 'LIKE', '%' . str_replace(' ', '', $keyword) . '%');
                });

		//has allowed tags
		if (strlen($user->allowed_tags) > 0) {
			$donors->whereRaw(' donors.id in (select donor_id from donor_tags where donor_id=donors.id and tag_id in ('.$user->allowed_tags.')) ');
		}

		$counter = clone $donors;
		$counter = $counter->get();

		$donors = $donors->take($per_page)->skip(($page-1) * $per_page)->get();
		//dd($donors);
		$totalPages = ceil(count($counter)/$per_page);
		$entries = count($counter);
		$dom = '';
		foreach($donors as $donor){
			$_donor = App\Donor::find($donor->donor_id);

			$donor_name = (strlen(trim($donor->first_name)) > 0) ? $donor->last_name . ', ' . $donor->first_name : $donor->last_name;

			$dom.='<tr>';
			$dom.='<td>';
			$dom.= $donor_name;
			$dom.='</td>';
			$dom.='<td>';
			$dom.= $donor->state;
			$dom.='</td>';
			$dom.='<td>';
			$dom.= $donor->zip;
			$dom.='</td>';
			$dom.='<td>';
			$dom.= App\Helpers\Data::formatPhone($donor->home_phone);
			$dom.='</td>';
			$dom.='<td>';
			$dom.= $donor->email;
			$dom.='</td>';
			$dom.='<td>';
			if($_donor){
				$dom.= money_format('%.2n',$_donor->getTotalDonations());
			}else{
				$dom.= '$ 0.00';
			}

			$dom.='</td>';
			$dom.='<td class="actions">';
			if ($user->edit_rights) {
				$dom.='<a href="' . url('/admin/donors/edit'). '/' . $donor->source . '">';
				$dom.='<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
				$dom.='</a>';
			}
			$dom.='<a href="' . url('/admin/donors/view'). '/' . $donor->source . '">';
			$dom.='<i class="fa fa-binoculars" aria-hidden="true"></i>';
			$dom.='</a>';
			if ($user->edit_rights) {
				$dom.='<a href="' . url('/admin/donors/delete'). '/' . $donor->source . '">';
				$dom.='<i class="fa fa-trash" aria-hidden="true"></i>';
				$dom.='</a>';
			}
			$dom.='</td>';
			$dom.='</tr>';
		}
		$pagination = '<ul class="pagination">';
		for($ndx=1; $ndx<=$totalPages; $ndx++){
			if($ndx == $page)
				$pagination.='<li class="active">';
			else
				$pagination.='<li>';
			$pagination.='<a data-row="'.$ndx.'">' . $ndx . '</a>';
			$pagination.='</li>';
		}
		$pagination.= '</ul>';
		return response()->json(['msg' => 'hi', '_dom' => $dom, 'pagination' => $pagination]);
	});

	//View Donor
	Route::get('/admin/donors/view/{id}',function($id){
		$_donor = App\Donor::find($id);
		$_tags = App\Tag::all();
		$tags = array();
		foreach($_tags as $tag){
			$tags[] = array(
				'id' => $tag->id,
				'text'	=> $tag->tag_name
			);
		}
		$donorTags = array();
		foreach($_donor->tags as $tag){
			$donorTags[] = $tag->tag_id;
		}

		return View::make('admin/donor/view',array('_donor'	=>	$_donor, '_tags' => json_encode($tags), '_dntags' => json_encode($donorTags), 'user' => Auth::user()));
	});

	//Insert Donor
	Route::match(['get', 'post'],'/admin/donors/add',['as' => 'insert_donor',function(){
		$_tags = App\Tag::all();
		$tags = array();
		foreach($_tags as $tag){
			$tags[] = array(
				'id' => $tag->id,
				'text'	=> $tag->tag_name
			);
		}
		if(Request::isMethod('POST')){
			$_donor = new App\Donor;
			$_donor->first_name = Input::get('first_name');
			$_donor->last_name = Input::get('last_name');
			$_donor->address1 = Input::get('address1');
			$_donor->address2 = Input::get('address2');
			$_donor->city = Input::get('city');
			$_donor->zip = Input::get('zip');
			$_donor->state = Input::get('state');
			$_donor->country = Input::get('country');
			$_donor->phone1 = preg_replace('/-/','',Input::get('phone1'));
			$_donor->phone2 = preg_replace('/-/','',Input::get('phone2'));
			$_donor->home_phone = preg_replace('/-/','',Input::get('home_phone'));
			$_donor->business_phone = preg_replace('/-/','',Input::get('business_phone'));
			$_donor->cellphone = preg_replace('/-/','',Input::get('cellphone'));
			$_donor->email = Input::get('email');
            $_donor->email2 = Input::get('email2');
			$_donor->employer_name = Input::get('employer_name');
			$_donor->notes = Input::get('notes');
			$_donor->occupation = Input::get('occupation');
            $_donor->donor_type = Input::get('donor_type');
            //$_donor->created_at = \Carbon\Carbon::parse(Input::get('created_at'))->format('m-d-Y');
			$_donor->save();

			return response()->json(['_redirect' => url('admin/donors')]);
		}
		return View::make('admin/donor/add', array('_donor' => new App\Donor, 'form_type' => 'New', 'post_url' => '/admin/donors/add', '_tags' => json_encode($tags)));
	}]);

	//Add Link donor
	Route::post('/admin/donors/{id}/link', ['as' => 'add_link_donor', function ($id, \Illuminate\Http\Request $request) {
		$donor = \App\Donor::find($request->get('id'));
		$primaryDonorId = \App\Donor::find($id);
		if ($donor && $primaryDonorId) {
			$donor->primary_donor_id = $id;
			$donor->save();

			return $donor;
		}
	}]);

	//Delete Link donor
	Route::delete('/admin/donors/{id}/link', ['as' => 'add_link_donor', function ($id, \Illuminate\Http\Request $request) {
		$donor = \App\Donor::where('id', $request->get('id'))->where('primary_donor_id', $id)->first();

		if ($donor) {
			$donor->primary_donor_id = null;
			$donor->save();
			return $donor;
		} else {
			return response()->json(['Donor not found'], 400);
		}
	}]);

	//Get link donors
	Route::get('/admin/donors/{id}/link', ['as' => 'get_link_donor', function ($id) {
		return \App\Donor::where('primary_donor_id', $id)
			->orderBy('updated_at', 'asc')
			->get();
	}]);

    //Edit contribution details
    Route::match(['post','get'],'/admin/donors/edit/contribution/{id}',['as' => 'edit_donation',function($id){
        $donation = App\DonorDonation::find($id);

        if(Request::isMethod('post')){
            $donation->donation_amount = Input::get('amount');
            $donation->donation_date = Carbon\Carbon::createFromFormat('m/d/Y', Input::get('date'));
            $donation->save();
            return redirect('admin/donors/edit/'.$donation->donor_id);
        }

        $response = array(
            'id' => $donation->id,
            'amount' => $donation->donation_amount,
            'donation_date' => date('m/d/Y', strtotime($donation->donation_date))
        );

        return View::make('admin/donor/editdonation', $response);
        //return response()->json($response);
    }]);

	//Update Donor
	Route::match(['post','get'],'/admin/donors/edit/{id}',function($id){
		$_donor = App\Donor::find($id);
		$_tags = App\Tag::all();
		$tags = array();
		foreach($_tags as $tag){
			$tags[] = array(
				'id' => $tag->id,
				'text'	=> $tag->tag_name
			);
		}
		$donorTags = array();
		foreach($_donor->tags as $tag){
			$donorTags[] = $tag->tag_id;
		}
		if(Request::isMethod('post')){
			$_donor->first_name = Input::get('first_name');
			$_donor->last_name = Input::get('last_name');
			$_donor->address1 = Input::get('address1');
			$_donor->address2 = Input::get('address2');
			$_donor->city = Input::get('city');
			$_donor->zip = Input::get('zip');
			$_donor->state = Input::get('state');
			$_donor->country = Input::get('country');
			$_donor->phone1 = preg_replace('/-/','',Input::get('phone1'));
			$_donor->phone2 = preg_replace('/-/','',Input::get('phone2'));
			$_donor->home_phone = preg_replace('/-/','',Input::get('home_phone'));
			$_donor->business_phone = preg_replace('/-/','',Input::get('business_phone'));
			$_donor->cellphone = preg_replace('/-/','',Input::get('cellphone'));
			$_donor->email = Input::get('email');
            $_donor->email2 = Input::get('email2');
            $_donor->donor_type = Input::get('donor_type');
			$_donor->employer_name = Input::get('employer_name');
			$_donor->notes = Input::get('notes');
			$_donor->occupation = Input::get('occupation');
            //$_donor->created_at = \Carbon\Carbon::parse(Input::get('created_at'))->format('m-d-Y');
			$_donor->save();
			//return redirect('/admin/donors');
		}
		return View::make('admin/donor/add', array('_donor' => $_donor, 'form_type' => 'Edit','_tags' => json_encode($tags), 'post_url' => '/admin/donors/edit/' . $id, '_dntags' => json_encode($donorTags)));
	});

	//Add donation
	Route::post('/admin/donor/donation/add',['as' => 'post_donation', function(){
		$amt = Input::get('amount');
		$donor = Input::get('donor');
		$date = Input::get('date');
		$_donation = new \App\DonorDonation;
		$_donation->donor_id = $donor;
		$_donation->donation_amount = $amt;
		$_donation->donation_date = Carbon\Carbon::createFromFormat('m/d/Y', $date);
		$_donation->save();
	}]);

	//Update donor tags
	Route::post('/admin/donor/tag/update',['as'	=> 'donor_tag_post',function(){
		$donorId = Input::get('donor');
		$tagId = Input::get('tag');
		$type = Input::get('type');
		if($type == 'insert'){
			$_donorTag = new App\DonorTag();
			$_donorTag->donor_id = $donorId;
			$_donorTag->tag_id = $tagId;
			$_donorTag->save();

		}elseif($type == 'delete'){
			$_donorTag = App\DonorTag::query()
				->where('tag_id','=',$tagId)
				->where('donor_id','=',$donorId)
				->first();
			$_donorTag->delete();
		}
		return response()->json(array('msg'	=> 'Success','code' => 1));
	}]);

	//Fetch Donation
	Route::post('/admin/donor/donation/fetch',['as' => 'fetch_donation',function(){
		$donor = Input::get('donor');
		$_donor = App\Donor::find($donor);
		$_dom = '';
		$total = money_format('%.2n',$_donor->getTotalDonations());
		foreach ($_donor->donations as $_donation){
			$_dom.= '<tr>';
				$_dom.= '<td>';
					$_dom.= money_format('%.2n',$_donation->donation_amount);
				$_dom.= '</td>';
				$_dom.= '<td>';
					$_dom.= Carbon\Carbon::parse($_donation->donation_date)->format('M d, Y (D)');
				$_dom.= '</td>';
                $_dom .= '<td><a data-toggle="modal" data-target="#donation_modal" href="/admin/donors/edit/contribution/'.$_donation->id.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>';
			$_dom.= '</tr>';
		}
		return response()->json(['msg' => '', 'code' => '', '_dom' => $_dom,'_total' => $total]);
	}]);

	//Advanced Search
	Route::get('/admin/advanced-search',['as' => 'advanced-search',function(){
		$_tags = App\Tag::all();
		$tags = array();
		foreach($_tags as $tag){
			$tags[] = array(
				'id' => $tag->id,
				'text'	=> $tag->tag_name
			);
		}
		$data = array(
			'min' => 1,
			'max' => 50000,
			'donors' => array(),
			'_tags'	=> json_encode($tags)
		);

		return View::make('admin/advanced_search/index',$data);
	}]);

	Route::post('/admin/advanced-search/q',['as' => 'post_advanced_search',function(){
		$user = Auth::user();

	    DB::enableQueryLog();

        $page = Input::get('page',1);
        $query = Input::get('query');

		$count = explode(',',Input::get('number_of_times',null));
		$donation = explode(',',Input::get('total_range', null));
		$total_amt_donated = explode(',',Input::get('max_range', null));
		$dom = '';

		$queryStr = '(`donors`.`first_name`  LIKE "%' . $query . '%" OR `donors`.`last_name` LIKE "%' . $query . '%" OR `donors`.`employer_name` LIKE "%' . $query . '%")';

        //donation between date
		$queryStr.='AND `donors`.`deleted_at` is null ';

        if (strlen(Input::get('period')) > 5) {
          $period = explode('-',Input::get('period'));
          $stdate = \Carbon\Carbon::parse($period[0])->format('Y-m-d');
          $endate = \Carbon\Carbon::parse($period[1])->format('Y-m-d');
          $queryStr.='AND DATE(donation_date) BETWEEN "' . $stdate .'" AND "' . $endate . '"';
        }

        //has tags
        if (count(Input::get('has_tags'))) {
            $queryStr.=' AND donors.id in (select donor_id from donor_tags where donor_id=donors.id and tag_id in ('.implode(Input::get('has_tags')).')) ';
        }

				//has allowed tags
				if (strlen($user->allowed_tags) > 0) {
					$queryStr.=' AND donors.id in (select donor_id from donor_tags where donor_id=donors.id and tag_id in ('.$user->allowed_tags.')) ';
				}

        //does not have tags
        if (count(Input::get('does_not_have_tags'))) {
            $queryStr.=' AND donors.id not in (select donor_id from donor_tags where donor_id=donors.id and tag_id in ('.implode(Input::get('does_not_have_tags')).')) ';
        }

        //zip code
		if (Input::get('zip_code') != '') {
		    if (strpos(Input::get('zip_code'), ',') > 0) {
		         $zips = explode(',', Input::get('zip_code'));
                 for ($i=0;$i<count($zips);$i++) {
                     $zips[$i] = trim($zips[$i]);
                 }
			     $queryStr.=' AND `donors`.`zip` in (' . implode(',', $zips). ')';
			} else {
			     $queryStr.=' AND `donors`.`zip` =\'' . Input::get('zip_code'). '\'';
			}
        }

        //county
        if(Input::get('county') != '')
            $queryStr.=' AND `donors`.`country` = \'' . Input::get('county'). '\'';

		$query = DB::table('donors')
			->select(DB::raw('*,`donors`.`id` as `source`'))
			->leftJoin('donor_donations','donors.id','=','donor_donations.donor_id')
			->whereRaw($queryStr)
			->groupBy('donors.id');

        //# of times donated
		if(strlen(Input::get('donation_times_min')) && strlen(Input::get('donation_times_max'))){
			//var_dump($count);
			$query->havingRaw('COUNT(donation_amount) BETWEEN '. (int)Input::get('donation_times_min') .' AND ' . (int)Input::get('donation_times_max'));
		}

        //lifetime donation amount
		if(strlen(Input::get('donation_total_min')) && strlen(Input::get('donation_total_max'))){
			$query->havingRaw('SUM(donation_amount) BETWEEN ' . (int)Input::get('donation_total_min') . ' AND ' . (int)Input::get('donation_total_max'));
		}

		$counter = clone $query;
		$sql = $counter->toSql();
		$counter = $counter->get();

		$totalPages = ceil(count($counter)/200);

		$results = $query->take(200)->skip(($page-1) * 200)->get();

        //print_r(DB::getQueryLog());
        //exit;

        if (count($results) <= 0) {
            $dom.='<tr><td colspan="7" style="text-align:center;">No matching donors found.</td></tr>';
        }

		$user = Auth::user();

		foreach($results as $result){
			$_donor = App\Donor::find($result->source);
			if($_donor){
				$dom.='<tr>';
					$dom.='<td>';
					$dom.='<input type="checkbox" name="multiaction" value="' . $result->source . '" class="chk-action">';
					$dom.='</td>';
					$dom.='<td>';
					$dom.= $result->first_name . ' ' . $result->last_name;
					$dom.='</td>';
					$dom.='<td>';
					$dom.= $result->employer_name;
					$dom.='</td>';
					$dom.='<td>';
					$dom.= money_format('%.2n', $_donor->getTotalDonations());
					$dom.='</td>';
					$dom.='<td>';
					$_donation = $_donor->getLastDonated();
					if($_donation)
						$dom.= \Carbon\Carbon::parse($_donation->donation_date)->format('M d, Y (D)');
					else
						$dom.='N/A';
					$dom.='</td>';
					$dom.='<td>';
					$dom.= $_donor->getNumberOfDonations();
					$dom.='</td>';
					if ($user->edit_rights) {
		                $dom.='<td class="actions">
                            <a target="_blank" href="/admin/donors/edit/'.$result->donor_id.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a target="_blank" href="/admin/donors/view/'.$result->donor_id.'"><i class="fa fa-binoculars" aria-hidden="true"></i></a>
                            <a href="/admin/donors/delete/'.$result->donor_id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>
                           </td>';
					} else {
						$dom.='<td class="actions">
										<a target="_blank" href="/admin/donors/view/'.$result->donor_id.'"><i class="fa fa-binoculars" aria-hidden="true"></i></a>
									 </td>';
					}
				$dom.='</tr>';
			}

		}
		$pagination = '<ul class="pagination">';
		for($ndx=1; $ndx<=$totalPages; $ndx++){
			if($ndx == $page)
				$pagination.='<li class="active">';
			else
				$pagination.='<li>';
			$pagination.='<a data-row="'.$ndx.'">' . $ndx . '</a>';
			$pagination.='</li>';
		}
		$pagination.= '</ul>';
		return response()->json(['_dom' => $dom, 'pagination' => $pagination, 'sql' => $sql, 'count' => number_format(count($counter))]);
	}]);

	//Export CSV
	Route::post('/admin/advacned-search/export',['as' => 'export_advanced_result',function(){
		$response = array(
			'path' => ''
		);
		$tags = DB::table('tags')
					->select('tag_name')
					->whereNull('deleted_at')
					->get();
		$tags =array_map(function($el){return $el->tag_name;},$tags);
		$tags = implode('?',$tags);
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$results = DB::select(Input::get('query'));
		DB::setFetchMode(PDO::FETCH_CLASS);

		$dir = date ('Y-m-d');
		$path = public_path() . DIRECTORY_SEPARATOR . $dir;
		if(!File::exists($path)) {
			File::makeDirectory($path);
		}
		$filename = 'export-search-' . date("Ymd") . '.csv';
		$filepath =  $path . DIRECTORY_SEPARATOR . $filename;
		$file = fopen($filepath,"w");
		$heading = 'First Name?Last Name?Address 1?Address 2?City?State?Zip?County?Home Phone?Business Phone?Cell Phone?Other Phone?Email?Alternate Email?Donor Type?Employer?Occupation?' . $tags;
		fputcsv($file,explode('?',$heading),',');
		foreach($results as $result){
			$fieldset = App\Helpers\Data::buildFieldset($result);
			fputcsv($file,$fieldset,',');
		}
		fclose($file);
		$response['path'] = asset($dir . '/' . $filename);
		return response()->json($response);
	}]);

	//Mass assignment
	Route::post('/admin/advanced-search/massupdate',['as' => 'mass_update',function(){
		$tags = Input::get('tags');
		$donors = Input::get('donors');
		foreach($tags as $tag){
			foreach ($donors as $donor){
				$rs = App\DonorTag::query()
					->where('donor_id','=',$donor)
					->where('tag_id','=', $tag)
					->get();
				if(count($rs) == 0){
					$_donorTag = new App\DonorTag();
					$_donorTag->donor_id = $donor;
					$_donorTag->tag_id = $tag;
					$_donorTag->save();
				}
			}
		}
	}]);

    //Get county
    Route::post('/admin/county',['as'=> 'post_county',function(){
        $zip = Input::get('zip','');
        $county ='N/A';
        if($zip!=""){
            $_county = App\County::where('zip','=', $zip)->first();
            if($_county)
                $county = $_county->county;

        }
        return response()->json(['county' => $county]);
    }]);
});


/* Actual Application routes */


Route::get('/admin/tags',['middleware' => 'auth',function(){
	$_tag = App\Tag::all();
	return View::make('admin/tag/view',array('tags' => $_tag));
}]);
Route::post('/admin/tags/find',['middleware' => 'auth',function(){
	$keyword = Input::get('keyword','');
	$results = App\Tag::where('tag_name','LIKE', '%' . $keyword . '%')->get();
	$dom = new \DOMDocument('1.0');
	foreach($results as $_tag){

		$tr = $dom->createElement('tr');

		//Build edit action element;
		$edit = $dom->createAttribute("href");
		$edit->value = url('/admin/tags/edit') .'/'. $_tag->id;
		$editAnchor = $dom->createElement('a');
		$editAnchor->appendChild($edit);
		$editIcon = $dom->createElement('i');
		$editIcon->setAttribute('class','fa fa-pencil-square-o');
		$editAnchor->appendChild($editIcon);

		//Build delete action element

		$deleteAnchor = $dom->createElement('a');
		$deleteAnchor->setAttribute('href', url('/admin/tags/delete/') .'/'. $_tag->id);
		$deleteIcon = $dom->createElement('i');
		$deleteIcon->setAttribute('class','fa fa-trash');
		$deleteAnchor->appendChild($deleteIcon);

		$td1 = $dom->createElement('td',$_tag->tag_name);
		$td2 = $dom->createElement('td',Carbon\Carbon::parse($_tag->created_at)->format('M d, Y (D)'));
		$td3 = $dom->createElement('td',Carbon\Carbon::parse($_tag->updated_at)->format('M d, Y (D)'));
		$td4 = $dom->createElement('td');
		$td4->setAttribute('class','actions');

		$td4->appendChild($editAnchor);
		$td4->appendChild($deleteAnchor);

		$tr->appendChild($td1);
		$tr->appendChild($td2);
		$tr->appendChild($td3);
		$tr->appendChild($td4);
		$dom->appendChild($tr);
	}
	$row = $dom->saveHTML();
	return response()->json(['msg' => json_encode($keyword), '_dom' => $row, 'code'	=> 1]);
}]);
Route::post('/admin/tags/add',['middleware' => 'auth', function(){
	$name = Input::get('tagname');
	$_tag = App\Tag::where('tag_name', '=', $name)->get();
	$response = "Tag already exists!";
	$code = 0;
	$row = '';
	if(count($_tag) == 0){
		$_tag = new App\Tag;
		$_tag->tag_name = $name;
		$_tag->save();
		$response = "New tag was added. ";
		$code = 1;
		$dom = new \DOMDocument('1.0');
		$tr = $dom->createElement('tr');
		$tr->setAttribute('class','actions');

		//Build edit action element;
		$edit = $dom->createAttribute("href");
		$edit->value = url('/admin/tags/edit') .'/'. $_tag->id;
		$editAnchor = $dom->createElement('a');
		$editAnchor->appendChild($edit);
		$editIcon = $dom->createElement('i');
		$editIcon->setAttribute('class','fa fa-pencil-square-o');
		$editAnchor->appendChild($editIcon);

		//Build delete action element

		$deleteAnchor = $dom->createElement('a');
		$deleteAnchor->setAttribute('href', url('/admin/tags/delete/') .'/'. $_tag->id);
		$deleteIcon = $dom->createElement('i');
		$deleteIcon->setAttribute('class','fa fa-trash');
		$deleteAnchor->appendChild($deleteIcon);



		$td1 = $dom->createElement('td',$_tag->tag_name);
		$td2 = $dom->createElement('td',Carbon\Carbon::parse($_tag->created_at)->format('M d, Y (D)'));
		$td3 = $dom->createElement('td',Carbon\Carbon::parse($_tag->updated_at)->format('M d, Y (D)'));
		$td4 = $dom->createElement('td');

		$td4->appendChild($editAnchor);
		$td4->appendChild($deleteAnchor);

		$tr->appendChild($td1);
		$tr->appendChild($td2);
		$tr->appendChild($td3);
		$tr->appendChild($td4);
		$dom->appendChild($tr);
		$row = $dom->saveHTML();

	}

	return response()->json(['msg' => $response, 'code' => $code,'_dom' => $row]);
}]);
Route::match(['post','get'],'admin/tags/edit/{id}',['middleware' => 'auth',function($id){
	$name = App\Tag::find($id);
	if(Request::isMethod('post')){
		$name->tag_name = Input::get('tagname');
		$name->update();
		return redirect('/admin/tags');
	}
	return View::make('admin/tag/add',array('_tag' => $name));
}]);
Route::get('admin/tags/delete/{id}',['middleware' => 'auth',function($id){
	$_tag = App\Tag::find($id);
	$_tag->delete();
	return redirect('/admin/tags');
}]);

Route::post('admin/donors/tag/add',['middleware' => 'auth', function(){
	$tags = Input::get('options', array());
	$owner = Input::get('owner');
	$_donor = App\Donor::find($owner);

	$assoctags =array();
	foreach($_donor->tags as $tag){
		$assoctags[] = $tag->tag_id;
	}
	//Add Tag
	foreach($tags as $tag){
		if(!in_array($tag['id'],$assoctags)){
			$_donorTag = new App\DonorTag;
			$_donorTag->tag_id = $tag['id'];
			$_donorTag->donor_id = $_donor->id;
			$_donorTag->save();
		}
	}
	//Remove Tag
	$_donor->load('tags');
	$assoctags = array();
	foreach($_donor->tags as $tag){
		$assoctags[] = $tag->tag_id;
	}
	foreach($assoctags as $tag){
		if(!in_array($tag, array_column($tags,'id'))){
			$_donorTag = App\DonorTag::where('donor_id','=', $owner)->where('tag_id','=', $tag);
			$_donorTag->delete();
		}
	}
}]);

Route::get('/admin/donors/delete/{id}',['middleware' => 'auth', function($id){
	$_donor = App\Donor::find($id);
	$_donor->delete();
	return redirect('/admin/donors');
}]);


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
