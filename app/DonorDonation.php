<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DonorDonation extends Model {

	//
    public $timestamps = false;
    public function donor(){
        return $this->belongsTo('App\Donor','donor_id');
    }
    
}
